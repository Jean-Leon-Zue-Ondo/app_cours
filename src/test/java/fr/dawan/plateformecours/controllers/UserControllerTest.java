//package fr.dawan.plateformecours.controllers;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.time.LocalDate;
//import java.util.List;
//import java.util.stream.Stream;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import fr.dawan.plateformecours.entities.User;
//import fr.dawan.plateformecours.services.UserService;
//import fr.dawan.plateformecours.tools.Tools;
//
//@WebMvcTest(UserController.class)
//public class UserControllerTest {
//
//    @Autowired
//    MockMvc mockMvc;
//    
//    @MockBean
//    UserService service;
//    
//    @ParameterizedTest
//    @MethodSource("allProvider")
//    void allTest(List<User> lstUser) throws Exception{
//        //List<User> lstUser= List.of(new User(),new User());
//        when(service.all()).thenReturn(lstUser);
//        
//        mockMvc.perform(
//                get("/manager/user")
//        ).andDo(print()).andExpect(status().isOk())
//        .andExpect(jsonPath("$").isNotEmpty())
//        .andExpect(jsonPath("$[0]['id']").value(lstUser.get(0).getId()))
//        .andExpect(jsonPath("$[1]['id']").value(lstUser.get(1).getId()))
//        .andExpect(jsonPath("$[2]['id']").value(lstUser.get(2).getId()))
//        
//        .andExpect(jsonPath("$[0]['nom']").value(lstUser.get(0).getNom()))
//        .andExpect(jsonPath("$[1]['nom']").value(lstUser.get(1).getNom()))
//        .andExpect(jsonPath("$[2]['nom']").value(lstUser.get(2).getNom()))
//        
//        .andExpect(jsonPath("$[0]['prenom']").value(lstUser.get(0).getPrenom()))
//        .andExpect(jsonPath("$[1]['prenom']").value(lstUser.get(1).getPrenom()))
//        .andExpect(jsonPath("$[2]['prenom']").value(lstUser.get(2).getPrenom()))
//        
//        .andExpect(jsonPath("$[0]['email']").value(lstUser.get(0).getEmail()))
//        .andExpect(jsonPath("$[1]['email']").value(lstUser.get(1).getEmail()))
//        .andExpect(jsonPath("$[2]['email']").value(lstUser.get(2).getEmail()))
//        
//        .andExpect(jsonPath("$[0]['dateNaissance']").value(lstUser.get(0).getDateNaissance()))
//        .andExpect(jsonPath("$[1]['dateNaissance']").value(lstUser.get(1).getDateNaissance()))
//        .andExpect(jsonPath("$[2]['dateNaissance']").value(lstUser.get(2).getDateNaissance()))
//        
//        .andExpect(jsonPath("$[0]['password']").value(lstUser.get(0).getPassword()))
//        .andExpect(jsonPath("$[1]['password']").value(lstUser.get(1).getPassword()))
//        .andExpect(jsonPath("$[2]['password']").value(lstUser.get(2).getPassword()))
//        
//        //.andExpect(jsonPath("$").value(Tools.toJson(lstUser)))
//        
//        ;
//        System.out.println(jsonPath("$"));
//    }
//    
//    @ParameterizedTest
//    @MethodSource("byIdProvider")
//    void byIdTest(User user, long id) throws Exception {
//        when(service.getById( anyLong() )).thenReturn(user);
//        mockMvc.perform(get("/manager/user/"+id))
//        .andDo(print()).andExpect(status().isOk())
//        .andExpect(jsonPath("$").isNotEmpty())
//        .andExpect(jsonPath("$.id").value(user.getId()))
//        .andExpect(jsonPath("$.nom").value(user.getNom()))
//        .andExpect(jsonPath("$.prenom").value(user.getPrenom()))
//        .andExpect(jsonPath("$.email").value(user.getEmail()))
//        .andExpect(jsonPath("$.dateNaissance").value(user.getDateNaissance()))
//        .andExpect(jsonPath("$.password").value(user.getPassword()))
//        //"{\"id\":3,\"nom\":null,\"prenom\":null,\"email\":null,\"dateNaissance\":null,\"password\":null}"
//        ;
//    } 
//    
//    @ParameterizedTest
//    @MethodSource("saveOrUpdateProvider")
//    void saveTest(User user) throws Exception {
//        when(service.saveOrUpdate(any(User.class))).thenReturn(user);
//        
//        mockMvc.perform(post("/manager/user").contentType(MediaType.APPLICATION_JSON).content(Tools.toJson(user)))
//        .andDo(print()).andExpect(status().isOk())
//        .andExpect(jsonPath("$").isNotEmpty())
//        .andExpect(jsonPath("$.id").value(user.getId()))
//        .andExpect(jsonPath("$.nom").value(user.getNom()))
//        .andExpect(jsonPath("$.prenom").value(user.getPrenom()))
//        .andExpect(jsonPath("$.email").value(user.getEmail()))
//        .andExpect(jsonPath("$.dateNaissance").value(user.getDateNaissance()))
//        .andExpect(jsonPath("$.password").value(user.getPassword()))
//        ;
//    }
//    
//    @ParameterizedTest
//    @MethodSource("saveOrUpdateProvider")
//    void updateTest(User user) throws Exception {
//        when(service.saveOrUpdate(any(User.class))).thenReturn(user);
//        
//        mockMvc.perform(put("/manager/user").contentType(MediaType.APPLICATION_JSON).content(Tools.toJson(user)))
//        .andDo(print()).andExpect(status().isOk())
//        .andExpect(jsonPath("$").isNotEmpty())
//        .andExpect(jsonPath("$.id").value(user.getId()))
//        .andExpect(jsonPath("$.nom").value(user.getNom()))
//        .andExpect(jsonPath("$.prenom").value(user.getPrenom()))
//        .andExpect(jsonPath("$.email").value(user.getEmail()))
//        .andExpect(jsonPath("$.dateNaissance").value(user.getDateNaissance()))
//        .andExpect(jsonPath("$.password").value(user.getPassword()))
//        ;
//    }
//    
//    @ParameterizedTest
//    @MethodSource("deleteProvider")
//    void deleteTest(long id) throws Exception {
//        doNothing().when(service).deleteById(anyLong());
//        
//        mockMvc.perform(delete("/manager/user/"+id))
//        .andDo(print()).andExpect(status().isOk())
//        ;
//    }
//    
//    
//    
//    private static Stream<Arguments> allProvider(){
//        User user1 = new User();
//        user1.setId(1L);
//        User user2 = new User("nom1","prenom1","nom.prenom@gmail.com",null,"password");
//        user2.setId(2L);
//        User user3 = new User("nom","prenom","nom.prenom@gmail.com",null,"password");
//        user3.setId(3L);
//        List<User> lstUser = List.of(user1,user1,user1);
//        List<User> lstUser2 = List.of(user2,user3,user2);
//        List<User> lstUser3 = List.of(user1, user2,user3);
//        
//        return Stream.of(
//                Arguments.of(lstUser),
//                Arguments.of(lstUser2),
//                Arguments.of(lstUser3)
//                );
//    }
//    
//    private static Stream<Arguments> byIdProvider(){
//        User user1 = new User();
//        user1.setId(1L);
//        User user2 = new User();
//        user2.setId(2L);
//        User user3 = new User("nom","prenom","nom.prenom@gmail.com",null,"password");
//        user3.setId(3L);
//        return Stream.of(
//                Arguments.of(user1,1L),
//                Arguments.of(user2,2L),
//                Arguments.of(user3,3L)
//                );
//    }
//    
//    private static Stream<Arguments> saveOrUpdateProvider(){
//        User user1 = new User();
//        user1.setId(1L);
//        User user2 = new User();
//        user2.setId(2L);
//        User user3 = new User("nom","prenom","nom.prenom@gmail.com",null,"password");
//        user3.setId(3L);
//        
//        return Stream.of(
//                Arguments.of(user1),
//                Arguments.of(user2),
//                Arguments.of(user3)
//                );
//    }
//    
//    private static Stream<Arguments> deleteProvider(){
//        return Stream.of(
//                Arguments.of(1L),
//                Arguments.of(2L),
//                Arguments.of(3L)
//                );
//    }
//}
