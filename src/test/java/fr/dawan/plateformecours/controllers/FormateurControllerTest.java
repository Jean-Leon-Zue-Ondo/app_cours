package fr.dawan.plateformecours.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.dawan.plateformecours.dto.FormationDto;
import fr.dawan.plateformecours.dto.users.FormateurDto;
import fr.dawan.plateformecours.entities.users.Formateur;
import fr.dawan.plateformecours.services.FormateurService;
import fr.dawan.plateformecours.services.FormationService;
import fr.dawan.plateformecours.tools.Tools;

@WebMvcTest(FormateurController.class)
public class FormateurControllerTest {

    @Autowired
    MockMvc mockMvc;
    
    @MockBean
    FormateurService service;
    
    @Test
    void allTest() throws Exception {
        List<FormateurDto> listF = List.of(new FormateurDto(),new FormateurDto());
        when(service.all()).thenReturn(listF);
        
        mockMvc.perform(
                get("/manager/formateur")
                ).andDo(print()).andExpect(status().isOk())
        //.andExpect(jsonPath("$").value(Tools.toJson(listF)))
        .andExpect(jsonPath(" ").isEmpty())
        ;
;    } 
    
    @Test
    void saveTest() throws JsonProcessingException, Exception {
        FormateurDto formateur = new FormateurDto();
        formateur.setId(1);
        when( service.saveOrUpdate(any(FormateurDto.class)) ).thenReturn(formateur);
        
        mockMvc.perform(post("/manager/formateur").contentType(MediaType.APPLICATION_JSON).content(Tools.toJson(formateur)))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1))
        ;
        
    }
    
    @Test
    void updateTest() throws JsonProcessingException, Exception {
        FormateurDto formateur = new FormateurDto();
        formateur.setId(1);
        when( service.saveOrUpdate(any(FormateurDto.class)) ).thenReturn(formateur);
        
        mockMvc.perform(put("/manager/formateur").contentType(MediaType.APPLICATION_JSON).content(Tools.toJson(formateur)))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1))
        ;
        
    }
    
    @Test
    void byIdTest() throws JsonProcessingException, Exception {
        FormateurDto formateur = new FormateurDto();
        formateur.setId(1);
        when( service.getById(anyLong()) ).thenReturn(formateur);
        
        mockMvc.perform(
                get("/manager/formateur/1"))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1))
        ;        
    }
    
    @Test
    void deleteById() throws Exception {
        
        String retour = "Formateur avec l'id = 1 est supprimer";
        when(service.deleteById(anyLong())).thenReturn(retour) ;
        
        mockMvc.perform(
                delete( "/manager/formateur/1" ))
        .andDo(print()).andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
        
        //.andExpect(jsonPath("$.id").value(1))
        //.andDo(verify(service).deleteById(1)) 
        ;
    }
    
//    public String toJson(Object objet) throws JsonProcessingException {
//        String result = Json.mapper().writeValueAsString(objet);
//        System.out.println(result);
//        return result;
//    }
}
