//package fr.dawan.plateformecours.services;
//
//import static org.mockito.Mockito.*;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import fr.dawan.plateformecours.entities.User;
//import fr.dawan.plateformecours.repositories.UserRepository;
//
//@ExtendWith(MockitoExtension.class)
//public class UserServiceTest {
//
//    private UserService service;
//    
//    @Mock
//    private UserRepository repo;
//    
//    @BeforeEach
//    void setup() {
//        service = new UserService(repo);
//    }
//    
//    @Test
//    void allTest(){
//        List<User> listuser = List.of(new User(),new User());
//        when(repo.findAll()).thenReturn(listuser);
//        service.all();
//        verify(repo).findAll();
//    }
//    
//    @Test
//    void saveOrUpdateTest() {
//        User user = new User();
//        when(repo.save(user)).thenReturn(user);
//        service.saveOrUpdate(user);
//        verify(repo).save(user);
//        
//    }
//    
//    @Test
//    void byIdTest() {
//        when(repo.findById(1L)).thenReturn(Optional.of(new User()));
//        service.getById(1L);
//        verify(repo).findById(1L);
//    }
//    
////    @Test
////    void byIdTest2() {
////        when(repo.findById(2L)).thenReturn(Optional.of(new User()));
////        service.byId(2L);
////        verify(repo).findById(2L);
////    }
//    
//    @Test
//    void deleteByIdTest() {
//        doNothing().when(repo).deleteById(1L);
//        service.deleteById(1L);
//        verify(repo).deleteById(1L);
//    }
//    
//    
//}
