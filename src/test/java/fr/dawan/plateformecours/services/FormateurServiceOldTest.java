//package fr.dawan.plateformecours.services;
//
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//
//import java.util.List;
//import java.util.Optional;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import fr.dawan.plateformecours.entities.users.Formateur;
//import fr.dawan.plateformecours.repositories.FormateurRepository;
//import fr.dawan.plateformecours.services.FormateurServiceOld;
//
//@ExtendWith(MockitoExtension.class)
//public class FormateurServiceOldTest {
//
//    private FormateurServiceOld service;
//    
//    @Mock
//    private FormateurRepository repo;
//    
//    @BeforeEach
//    void setup() {
//        service = new FormateurServiceOld(repo);
//    }
//    
//    
//    
//    @Test
//    void allTestSouldReturnEmptyList() {
//        List<Formateur> listFormateur = List.of();
//        when(repo.findAll()).thenReturn(listFormateur);
//        service.all();
//        verify(repo).findAll();
//    }
//    
//    @Test
//    void byIdTestShouldReturnNotEmpty() {
//        when(repo.findById(1L)).thenReturn(Optional.of(new Formateur()) );
//        
//        assertEquals(0L,service.getById(1L).getId());
//    }
//    
//    @Test
//    void byIdTestShouldReturnNull() {
//        when(repo.findById(1L)).thenReturn(Optional.empty());
//        
//        assertNull(service.getById(1L));
//    }
//    
//    @Test
//    void deleteByIdTest() {
//        doNothing().when(repo).deleteById(1L);
//        service.deleteById(1L);
//        verify(repo).deleteById(1L);
//    }
//    
//    
//    
//}
