package fr.dawan.plateformecours.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.UserDto;
import fr.dawan.plateformecours.entities.User;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.UserRepository;
import fr.dawan.plateformecours.tools.DtoTool;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserService extends GenericService<User, UserDto, Long, UserRepository> implements IUserService{


    public UserService(UserRepository repository) {
        super(repository, User.class, UserDto.class);
    }
    
    public UserDto getByEmailAndPassword(String email, String password) throws Exception{
        User user = repository.findByEmailPassword(email, password);
        
        return DtoTool.convert(user, UserDto.class);
    }
    
}
