package fr.dawan.plateformecours.services;

import fr.dawan.plateformecours.dto.users.EtudiantDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IEtudiantService extends IGenericService<EtudiantDto, Long> {

}
