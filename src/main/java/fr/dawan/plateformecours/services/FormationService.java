package fr.dawan.plateformecours.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.FormationDto;
import fr.dawan.plateformecours.entities.Formation;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.FormationRepository;
import fr.dawan.plateformecours.tools.DtoTool;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class FormationService extends GenericService<Formation, FormationDto, Long, FormationRepository> implements IFormationService{

    @Autowired
    private ICourService courService;

    public FormationService(FormationRepository repository) {
        super(repository, Formation.class, FormationDto.class); 
    }


}
