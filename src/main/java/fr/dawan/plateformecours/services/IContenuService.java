package fr.dawan.plateformecours.services;

import java.util.List;

import fr.dawan.plateformecours.dto.ContenuDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IContenuService extends IGenericService<ContenuDto, Long>{

    public List<ContenuDto> getByCourId(long courId) throws Exception;
    public void deleteByIdCour(long courId) throws Exception;
}
