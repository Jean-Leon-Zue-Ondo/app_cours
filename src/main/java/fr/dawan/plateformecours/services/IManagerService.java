package fr.dawan.plateformecours.services;

import fr.dawan.plateformecours.dto.users.ManagerDto;
import fr.dawan.plateformecours.entities.users.Manager;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IManagerService extends IGenericService<ManagerDto, Long> {

}
