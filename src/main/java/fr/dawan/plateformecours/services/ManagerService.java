package fr.dawan.plateformecours.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.users.ManagerDto;
import fr.dawan.plateformecours.entities.users.Manager;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.ManagerRepository;
import jakarta.transaction.Transactional;
@Service
@Transactional
public class ManagerService extends GenericService<Manager,ManagerDto, Long, ManagerRepository> implements IManagerService {

    public ManagerService(ManagerRepository repository) {
        super(repository, Manager.class, ManagerDto.class);
    }

	

}
