package fr.dawan.plateformecours.services;

import java.util.List;

import fr.dawan.plateformecours.dto.CourDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface ICourService extends IGenericService<CourDto, Long>{

    List<CourDto> getByFormateurId(long id) throws Exception;
    void deleteAllByFormateurId(long id) throws Exception;
}
