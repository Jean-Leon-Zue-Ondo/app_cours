package fr.dawan.plateformecours.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.CourDto;
import fr.dawan.plateformecours.entities.Cour;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.CourRepository;
import fr.dawan.plateformecours.tools.DtoTool;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class CourService extends GenericService<Cour, CourDto, Long, CourRepository> implements ICourService {
    
    @Autowired
    private IContenuService contenuService;

    public CourService(CourRepository repository) {
        super(repository, Cour.class, CourDto.class);
    }

    @Override
    public List<CourDto> getByFormateurId(long id) throws Exception{
        
        List<CourDto> result = new ArrayList<>();
    
        List<Cour> lstC = repository.findAllByFormateurId(id);
        
        for(Cour c : lstC) {
            result.add(DtoTool.convert(c, CourDto.class));
        }
        
        
        
        
        return result;
    }

    @Override
    public void deleteAllByFormateurId(long id) throws Exception {
        
        repository.deleteAllByFormateurId(id);
    }

    @Override
    public String deleteById(Long id) throws Exception {
       
        return super.deleteById(id);
    }

    
   
}
