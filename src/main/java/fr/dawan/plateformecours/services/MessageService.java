package fr.dawan.plateformecours.services;

import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.MessageDto;
import fr.dawan.plateformecours.entities.Message;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.MessageRepository;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class MessageService extends GenericService<Message, MessageDto, Long, MessageRepository> implements IMessageService{

    public MessageService(MessageRepository repository) {
        super(repository, Message.class, MessageDto.class);
    }

    

}
