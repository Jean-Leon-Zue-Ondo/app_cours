package fr.dawan.plateformecours.services;

import java.util.List;

import fr.dawan.plateformecours.dto.UserDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IUserService extends IGenericService<UserDto, Long>{


    public UserDto getByEmailAndPassword(String email, String password) throws Exception;
}
