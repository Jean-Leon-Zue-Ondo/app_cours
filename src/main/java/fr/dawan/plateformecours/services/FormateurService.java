package fr.dawan.plateformecours.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.users.FormateurDto;
import fr.dawan.plateformecours.entities.users.Formateur;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.FormateurRepository;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class FormateurService extends GenericService<Formateur, FormateurDto, Long, FormateurRepository> implements IFormateurService{

    public FormateurService(FormateurRepository repository) {
        super(repository, Formateur.class, FormateurDto.class);
    }

}
