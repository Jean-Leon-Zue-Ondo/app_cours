package fr.dawan.plateformecours.services;

import fr.dawan.plateformecours.dto.users.FormateurDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IFormateurService extends IGenericService<FormateurDto, Long>{

}
