package fr.dawan.plateformecours.services;

import java.util.List;

import fr.dawan.plateformecours.dto.FormationDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IFormationService extends IGenericService<FormationDto, Long> {

    
}
