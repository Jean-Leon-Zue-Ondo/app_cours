package fr.dawan.plateformecours.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.ContenuDto;
import fr.dawan.plateformecours.entities.Contenu;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.ContenuRepository;
import fr.dawan.plateformecours.tools.DtoTool;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class ContenuService extends GenericService<Contenu, ContenuDto, Long, ContenuRepository> implements IContenuService{

    public ContenuService(ContenuRepository repository) {
        super(repository, Contenu.class, ContenuDto.class);
    }

    @Override
    public List<ContenuDto> getByCourId(long courId) throws Exception {
        List<ContenuDto> result = new ArrayList<>();
        List<Contenu> lstC = repository.findByCourId(courId);
        
        for(Contenu c : lstC) {
            result.add(DtoTool.convert(c, ContenuDto.class));
        }
        
        return result;
    }

    @Override
    public void deleteByIdCour(long courId) throws Exception {
        repository.deleteAllByCourId(courId);
        
    }

    

}
