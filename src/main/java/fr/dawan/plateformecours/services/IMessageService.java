package fr.dawan.plateformecours.services;

import fr.dawan.plateformecours.dto.MessageDto;
import fr.dawan.plateformecours.generic.IGenericService;

public interface IMessageService extends IGenericService<MessageDto, Long>{

}
