package fr.dawan.plateformecours.services;


import org.springframework.stereotype.Service;

import fr.dawan.plateformecours.dto.users.EtudiantDto;
import fr.dawan.plateformecours.entities.users.Etudiant;
import fr.dawan.plateformecours.generic.GenericService;
import fr.dawan.plateformecours.repositories.EtudiantRepository;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class EtudiantService extends GenericService<Etudiant, EtudiantDto, Long, EtudiantRepository> implements IEtudiantService{
    
    
    public EtudiantService(EtudiantRepository repository) {
        super(repository, Etudiant.class, EtudiantDto.class);
    }

    


    

}
