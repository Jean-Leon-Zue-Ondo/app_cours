package fr.dawan.plateformecours.dto;

import java.time.LocalDate;
import java.util.List;

public class FormationDto {

    private long id;
    private int version;
    private String nomFormation;
    private LocalDate dateDebut;
    private LocalDate dateFin;
    private int nbCours;
    private List<CourDto> cours;
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }
    public String getNomFormation() {
        return nomFormation;
    }
    public void setNomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
    }
    public List<CourDto> getCours() {
        return cours;
    }
    public void setCours(List<CourDto> cours) {
        this.cours = cours;
    }
    public LocalDate getDateDebut() {
        return dateDebut;
    }
    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }
    public LocalDate getDateFin() {
        return dateFin;
    }
    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
    public int getNbCours() {
        return nbCours;
    }
    public void setNbCours(int nbCours) {
        this.nbCours = nbCours;
    }
    
   
    
    
    
}
