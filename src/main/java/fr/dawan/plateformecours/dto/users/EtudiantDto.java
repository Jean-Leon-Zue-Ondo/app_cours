package fr.dawan.plateformecours.dto.users;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.plateformecours.dto.ContenuDto;
import fr.dawan.plateformecours.dto.UserDto;

public class EtudiantDto extends UserDto{

   
    //private long formationId;
    private String formationNom;
    private List<ContenuDto> contenu;

    

   /* public long getFormationId() {
        return formationId;
    }

    public void setFormationId(long formationId) {
        this.formationId = formationId;
    }*/

    public List<ContenuDto> getContenu() {
        return contenu;
    }

    public void setContenu(List<ContenuDto> contenu) {
        this.contenu = contenu;
    }

    public String getFormationNom() {
        return formationNom;
    }

    public void setFormationNom(String formationNom) {
        this.formationNom = formationNom;
    }

    

    
    
    
}
