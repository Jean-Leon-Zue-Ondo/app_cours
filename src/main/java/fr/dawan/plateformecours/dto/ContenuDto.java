package fr.dawan.plateformecours.dto;

import fr.dawan.plateformecours.dto.users.EtudiantDto;
import fr.dawan.plateformecours.dto.users.FormateurDto;

public class ContenuDto {
    private long id;
    private int version;
    private String titre;
    private String description;
    private String path;
    private long formateurId;
    private String formateurNom;
    private String formateurPrenom;
    private long courId;
    private String courNomCour;
    
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public long getFormateurId() {
        return formateurId;
    }
    public void setFormateurId(long formateurId) {
        this.formateurId = formateurId;
    }
    public String getFormateurNom() {
        return formateurNom;
    }
    public void setFormateurNom(String formateurNom) {
        this.formateurNom = formateurNom;
    }
    public String getFormateurPrenom() {
        return formateurPrenom;
    }
    public void setFormateurPrenom(String formateurPrenom) {
        this.formateurPrenom = formateurPrenom;
    }
    public long getCourId() {
        return courId;
    }
    public void setCourId(long courId) {
        this.courId = courId;
    }
    public String getCourNomCour() {
        return courNomCour;
    }
    public void setCourNomCour(String courNomCour) {
        this.courNomCour = courNomCour;
    }
    
    
    
    
    

}

