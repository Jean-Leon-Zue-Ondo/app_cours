package fr.dawan.plateformecours.dto;

public class MessageDto {

    private long idmessage;
    private UserDto destinataire;
    private UserDto expediteur;
    private String message;
    
    public long getIdmessage() {
        return idmessage;
    }
    public void setIdmessage(long idmessage) {
        this.idmessage = idmessage;
    }
    public UserDto getDestinataire() {
        return destinataire;
    }
    public void setDestinataire(UserDto destinataire) {
        this.destinataire = destinataire;
    }
    public UserDto getExpediteur() {
        return expediteur;
    }
    public void setExpediteur(UserDto expediteur) {
        this.expediteur = expediteur;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
