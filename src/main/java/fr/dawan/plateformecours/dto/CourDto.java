package fr.dawan.plateformecours.dto;

import fr.dawan.plateformecours.dto.users.FormateurDto;

public class CourDto {
    private long id;
    private String nomCour;
    private CourFormateurDto formateur;
    
    private long formationId;
    private String formationNomFormation;

    public CourFormateurDto getFormateur() {
        return formateur;
    }
    public void setFormateur(CourFormateurDto formateur) {
        this.formateur = formateur;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getNomCour() {
        return nomCour;
    }
    public void setNomCour(String nomCour) {
        this.nomCour = nomCour;
    }
    public long getFormationId() {
        return formationId;
    }
    public void setFormationId(long formationId) {
        this.formationId = formationId;
    }
    public String getFormationNomFormation() {
        return formationNomFormation;
    }
    public void setFormationNomFormation(String formationNomFormation) {
        this.formationNomFormation = formationNomFormation;
    }
    
    
    
    

}
