package fr.dawan.plateformecours.entities;

import fr.dawan.plateformecours.entities.users.Formateur;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Version;

@Entity
public class Contenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Version
    private int version;
    
    private String titre;
    @Column(length = 2500)
    private String description;
    private String path;
    
    @ManyToOne
    private Formateur formateur;
    
    @ManyToOne
    private Cour cour;
    
    

    public Contenu() {
        
    }

    
    public Contenu(String titre, String description, String path, Formateur formateur, Cour cour) {
        super();
        this.titre = titre;
        this.description = description;
        this.path = path;
        this.formateur = formateur;
        this.cour = cour;
    }



    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }


    public int getVersion() {
        return version;
    }


    public void setVersion(int version) {
        this.version = version;
    }


    public Cour getCour() {
        return cour;
    }


    public void setCour(Cour cour) {
        this.cour = cour;
    }

    
    
    
    
    
    
}
