package fr.dawan.plateformecours.entities;

import java.util.List;

import fr.dawan.plateformecours.entities.users.Formateur;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Version;

@Entity
public class Cour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idCour")
    private long id;
    @Version
    private int version;
    @Column(nullable = false)
    private String nomCour;
    @OneToMany(mappedBy = "cour", cascade = CascadeType.REMOVE)
    private List<Contenu> contenu;
    
    @ManyToOne
    private Formateur formateur;
    
    @ManyToOne
    private Formation formation;
    
    public Cour() {
        
    }

    public Cour(String nomCour, Formateur formateur, Formation formation) {
        this.nomCour = nomCour;
        this.formateur = formateur;
        this.formation = formation;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomCour() {
        return nomCour;
    }

    public void setNomCour(String nomCour) {
        this.nomCour = nomCour;
    }

    public Formateur getFormateur() {
        return formateur;
    }

    public void setFormateur(Formateur formateur) {
        this.formateur = formateur;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }
    
   
    
}
