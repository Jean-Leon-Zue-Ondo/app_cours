package fr.dawan.plateformecours.entities.users;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.plateformecours.entities.Cour;
import fr.dawan.plateformecours.entities.User;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Email;

@Entity
public class Formateur extends User {
    
   @OneToMany(mappedBy = "formateur")
   private List<Cour> cours;
   
//   public Formateur(String nom, String prenom, @Email String email, LocalDate dateNaissance, String password) {
//       super(nom, prenom, email, dateNaissance, password);
//       this.cours = new ArrayList<Cour>();
//   }
   public Formateur() {
       super();
   }

    public Formateur(String nom, String prenom, @Email String email, LocalDate dateNaissance, String password) {
        super(nom, prenom, email, dateNaissance, password);

    }

    public List<Cour> getCours() {
        return cours;
    }

    public void setCours(List<Cour> cours) {
        this.cours = cours;
    }
    

    

}
