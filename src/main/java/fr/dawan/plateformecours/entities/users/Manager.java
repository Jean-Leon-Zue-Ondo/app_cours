package fr.dawan.plateformecours.entities.users;

import java.time.LocalDate;

import fr.dawan.plateformecours.entities.User;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.Email;

@Entity
public class Manager extends User {
    
    public Manager() {
        super();
    }

    public Manager(String nom, String prenom, @Email String email, LocalDate dateNaissance, String password) {
        super(nom, prenom, email, dateNaissance, password);
        // TODO Auto-generated constructor stub
    }

    
}
