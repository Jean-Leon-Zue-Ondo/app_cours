package fr.dawan.plateformecours.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.plateformecours.entities.users.Etudiant;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Version;

@Entity
public class Formation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idFormation")
    private long id;
    @Column(nullable = false)
    private String nomFormation;
    
    @Version
    private int version;
    
    @Temporal(TemporalType.DATE)
    private LocalDate dateDebut;
    @Temporal(TemporalType.DATE)
    private LocalDate dateFin;
    
    @OneToMany(mappedBy = "formation", cascade = CascadeType.REMOVE)
    private List<Cour> cours;
    
    @OneToMany(mappedBy = "formation", cascade = CascadeType.REMOVE)
    private List<Etudiant> etudiant;
    
    public Formation() {
        
    }
    
    

    public Formation(String nomFormation) {
        super();
        this.nomFormation = nomFormation;
    }


    

    public Formation(String nomFormation, LocalDate dateDebut, LocalDate dateFin) {
        super();
        this.nomFormation = nomFormation;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }



    public Formation(String nomFormation, List<Cour> cours) {
        super();
        this.nomFormation = nomFormation;
        this.cours = cours;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomFormation() {
        return nomFormation;
    }

    public void setNomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
    }

    public List<Cour> getCours() {
        return cours;
    }

    public void setCours(List<Cour> cours) {
        this.cours = cours;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }



    public LocalDate getDateDebut() {
        return dateDebut;
    }



    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }



    public LocalDate getDateFin() {
        return dateFin;
    }



    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }
    
    public int getNbCours() {
    if(cours != null)
        return cours.size();
    else
        return 0;
    }
    
    
}
