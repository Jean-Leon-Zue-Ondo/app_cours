package fr.dawan.plateformecours.tools;

import org.modelmapper.ModelMapper;

public class DtoTool {
    
    private static ModelMapper mapper = new ModelMapper();
    
    //Docs: https://modelmapper.org/getting-started/
    
//  public static Product convertToProduct(ProductDto dto) {
//      return mapper.map(dto, Product.class);
//  }
//  
//  public static ProductDto convertToProduct(Product p) {
//      return mapper.map(p, ProductDto.class);
//  }
    
    /**
     * Méthode qui convertie une entity en dto et vice versa
     * @param <TSource> Type Source
     * @param <TDestination> Type destination
     * @param obj Objet à convertir
     * @param clazz Type de l'objet convertis
     * @return L'objet convertis
     */
    public static <TSource, TDestination> TDestination convert(TSource obj, Class<TDestination> clazz) {
        
        //Ajouter des règles personnalisées
//      mapper.typeMap(Product.class, ProductDto.class)
//      .addMappings(mp -> 
//                  mp.map(src -> src.getDescription(), ProductDto::setDescription)
//              );
        
        
        return mapper.map(obj, clazz);
    }

}
