package fr.dawan.plateformecours.tools;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.v3.core.util.Json;

public class Tools {

    
    public static String toJson(Object objet) throws JsonProcessingException {
        String result = Json.mapper().writeValueAsString(objet);
        System.out.println(result);
        return result;
    }
}
