package fr.dawan.plateformecours.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.users.FormateurDto;
import fr.dawan.plateformecours.entities.users.Formateur;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.FormateurRepository;
import fr.dawan.plateformecours.services.IFormateurService;



@RestController
@RequestMapping("manager/formateur")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class FormateurController extends GenericController<Formateur,FormateurDto, Long, FormateurRepository, IFormateurService> {

    public FormateurController(IFormateurService service) {
        super(service);
    }

    
   
}
