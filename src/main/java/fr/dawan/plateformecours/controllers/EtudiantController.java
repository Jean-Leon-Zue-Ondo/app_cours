package fr.dawan.plateformecours.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.users.EtudiantDto;
import fr.dawan.plateformecours.entities.users.Etudiant;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.EtudiantRepository;
import fr.dawan.plateformecours.services.IEtudiantService;



@RestController
@RequestMapping("etudiant")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class EtudiantController extends GenericController<Etudiant, EtudiantDto, Long, EtudiantRepository, IEtudiantService>{

    public EtudiantController(IEtudiantService service) {
        super(service);
        // TODO Auto-generated constructor stub
    }



    
    

}
