package fr.dawan.plateformecours.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.FormationDto;
import fr.dawan.plateformecours.entities.Formation;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.FormationRepository;
import fr.dawan.plateformecours.services.IFormationService;

@RestController
@RequestMapping("formation")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class FormationController extends GenericController<Formation, FormationDto, Long, FormationRepository, IFormationService>{

    public FormationController(IFormationService service) {
        super(service);
    }
    
    
   
    
}
