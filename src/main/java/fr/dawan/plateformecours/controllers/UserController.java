package fr.dawan.plateformecours.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.UserDto;
import fr.dawan.plateformecours.entities.User;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.UserRepository;
import fr.dawan.plateformecours.services.IUserService;

@RestController
@RequestMapping("user")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class UserController extends GenericController<User, UserDto, Long, UserRepository, IUserService> {

    
    public UserController(IUserService service) {
        super(service);
    }
    
    @GetMapping(value = "/login", produces = "application/json")
    public UserDto getByEmailAndPassword(@RequestParam("email")String email, @RequestParam("password") String password) {
        try {
            return service.getByEmailAndPassword(email,password);
        } catch (Exception e) {
            return new UserDto();
        }
        
    }
}
