package fr.dawan.plateformecours.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.plateformecours.dto.ContenuDto;
import fr.dawan.plateformecours.entities.Contenu;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.ContenuRepository;
import fr.dawan.plateformecours.services.IContenuService;

@RestController
@RequestMapping("contenu")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class ContenuController extends GenericController<Contenu, ContenuDto, Long, ContenuRepository, IContenuService>{

    @Value("${storage.folder}")
    private String storageFolder;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    public ContenuController(IContenuService service) {
        super(service);
    }
    
    




    @PostMapping(value = "/file" ,produces = "application/json", consumes = "multipart/form-data")
    public ResponseEntity<ContenuDto> save(@RequestParam(name = "id") long id, @RequestParam(name = "file", required = false) MultipartFile file) throws Exception {
       // ContenuDto cDto = objectMapper.readValue(contenuDtoString, ContenuDto.class);
        ContenuDto cDto = service.getById(id);
        if(file!=null && !file.isEmpty()) {
            String filePath = "/" + LocalDate.now()+"-"+file.getOriginalFilename();
            File f = new File(storageFolder+filePath);
            try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f))){
                bos.write(file.getBytes());
            }
            cDto.setPath(filePath);
        }
        
        
        
        ContenuDto dtoSaved = service.saveOrUpdate(cDto);
        
        return ResponseEntity.ok(dtoSaved);
    }

    
    @PutMapping(value = "/file" ,produces = "application/json", consumes = "multipart/form-data")
    public ResponseEntity<ContenuDto> update(@RequestParam("contenuDtoString") String contenuDtoString, @RequestParam(name = "file", required = false) MultipartFile file) throws Exception {
        ContenuDto cDto = objectMapper.readValue(contenuDtoString, ContenuDto.class);
        
        if(file!=null && !file.isEmpty()) {
            String filePath = "/" + LocalDate.now()+"-"+file.getOriginalFilename();
            File old = new File(storageFolder+cDto.getPath());
            old.delete();
            File f = new File(storageFolder+filePath);
            try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f))){
                bos.write(file.getBytes());
            }
            cDto.setPath(filePath);
        }
        ContenuDto dtoSaved = service.saveOrUpdate(cDto);
        
        return ResponseEntity.ok(dtoSaved);
    }
    
    @GetMapping(value="/file/{contenuId}")
    public ResponseEntity<Resource> getContenuFile(@PathVariable("contenuId") long id) throws Exception{
        ContenuDto cdto = service.getById(id);
        if(cdto.getPath()==null) {
            return ResponseEntity.ok().body(null);
        }
        Resource resource = null;
        try {
            Path path = Paths.get(".").resolve(storageFolder+cdto.getPath());
            resource = new UrlResource(path.toUri());
        } catch (Exception e) {
            throw new RuntimeException();
        }
        
        Path newPath = resource.getFile().toPath();
        
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\""+resource.getFilename()+"\"")
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(newPath))
                .body(resource);
    }


    @Override
    @DeleteMapping(value="/{id}", produces = "text/plain")
    public ResponseEntity<String> deleteById(@PathVariable Long id) throws Exception {
        ContenuDto dto = service.getById(id);
        if(dto.getPath()!=null && !dto.getPath().isEmpty()) {
            File old = new File(storageFolder+dto.getPath());
            old.delete();
        }
        return super.deleteById(id);
    }
    
    @GetMapping(value = "/formateur",produces = "application/json")
    public List<ContenuDto> getByCourId(@RequestParam long courId){
        try {
            return service.getByCourId(courId);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

}
