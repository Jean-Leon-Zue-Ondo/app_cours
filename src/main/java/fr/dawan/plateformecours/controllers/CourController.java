package fr.dawan.plateformecours.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.CourDto;
import fr.dawan.plateformecours.entities.Cour;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.CourRepository;
import fr.dawan.plateformecours.services.ICourService;
import jakarta.websocket.server.PathParam;

@RestController
@RequestMapping("/cours")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class CourController extends GenericController<Cour, CourDto, Long, CourRepository, ICourService> {

    public CourController(ICourService service) {
        super(service);
        
        
        
    }
    @GetMapping(value= "/formateur",produces = "application/json")
    public List<CourDto> allFormateur(@PathParam("formateurId") long formateurId){
        
            try {
                return service.getByFormateurId(formateurId);
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }
}
