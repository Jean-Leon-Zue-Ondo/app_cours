package fr.dawan.plateformecours.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.MessageDto;
import fr.dawan.plateformecours.entities.Message;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.MessageRepository;
import fr.dawan.plateformecours.services.IMessageService;

@RestController
@RequestMapping("message")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class MessageController extends GenericController<Message, MessageDto, Long, MessageRepository, IMessageService> {

    public MessageController(IMessageService service) {
        super(service);
    }

}
