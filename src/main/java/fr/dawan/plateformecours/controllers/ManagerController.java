package fr.dawan.plateformecours.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.plateformecours.dto.users.ManagerDto;
import fr.dawan.plateformecours.entities.users.Manager;
import fr.dawan.plateformecours.generic.GenericController;
import fr.dawan.plateformecours.repositories.ManagerRepository;
import fr.dawan.plateformecours.services.IManagerService;
@RestController
@RequestMapping("manager/managers")
@CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
public class ManagerController extends GenericController<Manager,ManagerDto, Long, ManagerRepository, IManagerService> {

	public ManagerController(IManagerService service) {
		super(service);
	}

}
