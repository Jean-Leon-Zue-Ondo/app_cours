package fr.dawan.plateformecours.generic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.tools.DtoTool;
import jakarta.transaction.Transactional;

@Transactional
public abstract class GenericService<Entity,DTO, ID, Repository extends JpaRepository<Entity,ID>> implements IGenericService<DTO,ID>{

    protected final Repository repository;
    protected final Class<Entity> entityClass;
    protected final Class<DTO> dtoClass;
    
    
    
    public GenericService(Repository repository, Class<Entity> entityClass, Class<DTO> dtoClass) {
        this.repository = repository;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    @Override
    public List<DTO> all() throws Exception {
        List<DTO> result = new ArrayList<>();
        List<Entity> prods = repository.findAll();
        
        for(Entity e : prods) {
            result.add(DtoTool.convert(e,this.dtoClass ));
        }
        
        return result;
    }
    
    @Override
    public DTO saveOrUpdate(DTO dto) throws Exception {
        Entity e = DtoTool.convert(dto, this.entityClass);
        Entity saved = repository.saveAndFlush(e);
        return DtoTool.convert(saved, dtoClass);
    }
    
    @Override
    public DTO getById(ID id)throws Exception  {
        Optional<Entity> prodE = repository.findById(id);
        if(prodE.isPresent()) {
            return DtoTool.convert(prodE, dtoClass);
        }
        return null;
    }
    
    @Override
    public String deleteById(ID id) throws Exception {
        String str = entityClass.getSimpleName();
        repository.deleteById(id);
        return str;
    }
    
}
