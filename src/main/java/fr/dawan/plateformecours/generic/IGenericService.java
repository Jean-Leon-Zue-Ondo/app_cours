package fr.dawan.plateformecours.generic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IGenericService<DTO,ID> {

    List<DTO> all() throws Exception;

    DTO saveOrUpdate(DTO dto) throws Exception;

    DTO getById(ID id) throws Exception;

    String deleteById(ID id) throws Exception;

    
}
