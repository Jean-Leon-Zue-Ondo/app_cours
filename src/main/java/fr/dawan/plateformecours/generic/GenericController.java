package fr.dawan.plateformecours.generic;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


public abstract class GenericController<Entity,DTO, ID, Repository extends JpaRepository<Entity,ID>, IService extends IGenericService<DTO,ID>> {

    protected final IService service;

    public GenericController(IService service) {
        super();
        this.service = service;
    }
    
    @GetMapping(produces = "application/json")
    public List<DTO> all() throws Exception{
        return service.all();
    }
    
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<DTO> save(@RequestBody DTO dto) throws Exception {
        DTO dtoAdd = service.saveOrUpdate(dto);
        return ResponseEntity.ok(dtoAdd);
    }
    
    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<DTO> update(@RequestBody DTO dto) throws Exception {
        DTO dtoSaved = service.saveOrUpdate(dto);
        return ResponseEntity.ok(dtoSaved);
    }
    
    @GetMapping(value = "/{id}", produces = "application/json")
    public DTO getById(@PathVariable ID id) throws Exception {
        return service.getById(id);
    }
    
    @DeleteMapping(value = "/{id}", produces = "text/plain")
    public ResponseEntity<String> deleteById(@PathVariable ID id) throws Exception {
        String name = service.deleteById(id);
        return ResponseEntity.ok(name+" avec l'id = "+id+" est supprimer");
    }
}
