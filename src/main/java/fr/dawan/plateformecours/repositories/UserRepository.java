package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.plateformecours.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("From User u WHERE u.email = :email AND u.password=:password")
    User findByEmailPassword(@Param("email") String email, @Param("password") String password);
}
