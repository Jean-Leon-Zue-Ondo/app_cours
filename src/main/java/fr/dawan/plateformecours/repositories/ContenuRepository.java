package fr.dawan.plateformecours.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.plateformecours.entities.Contenu;

public interface ContenuRepository extends JpaRepository<Contenu, Long>{

    
    @Query("From Contenu WHERE cour.id = :courId")
    List<Contenu> findByCourId( @Param("courId") long courId);
    
    @Query(value = "DELETE FROM Contenu c WHERE c.cour.id = :courId")
    void deleteAllByCourId(@Param("courId") long courId);
}
