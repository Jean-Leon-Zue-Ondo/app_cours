package fr.dawan.plateformecours.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.plateformecours.entities.Cour;

public interface CourRepository extends JpaRepository<Cour, Long> {

    @Query(value = "From Cour c Where c.formateur.id = :id")
    List<Cour> findAllByFormateurId(@Param("id") long id);
    

    @Query(value = "DELETE FROM Cour c WHERE c.formateur.id = :id")
    void deleteAllByFormateurId(@Param("id") long id);
}
