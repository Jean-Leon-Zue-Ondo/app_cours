package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.entities.Formation;

public interface FormationRepository extends JpaRepository<Formation, Long>{

}
