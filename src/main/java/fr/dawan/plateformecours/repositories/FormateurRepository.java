package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.entities.users.Formateur;

public interface FormateurRepository extends JpaRepository<Formateur, Long> {

}
