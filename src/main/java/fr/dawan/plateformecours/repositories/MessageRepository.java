package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.entities.Message;

public interface MessageRepository extends JpaRepository<Message, Long>{

}
