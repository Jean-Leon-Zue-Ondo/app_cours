package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.entities.users.Manager;

public interface ManagerRepository extends JpaRepository<Manager, Long> {

}
