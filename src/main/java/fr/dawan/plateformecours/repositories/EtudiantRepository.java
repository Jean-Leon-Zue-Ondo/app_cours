package fr.dawan.plateformecours.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.plateformecours.entities.users.Etudiant;

public interface EtudiantRepository extends JpaRepository<Etudiant, Long>{

}
